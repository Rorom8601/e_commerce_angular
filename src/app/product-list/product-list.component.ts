import { Component, OnInit } from '@angular/core';
import { Page, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products?:Page<Product>;
  page = 0;
  pageSize = 10;

  constructor(private ps: ProductService) { }

  ngOnInit(): void {
    this.fetchProducts();
  }

  nextPage() {
    this.page++;
    this.fetchProducts();
  }
  previousPage() {
    this.page--;
    this.fetchProducts();
  }

  fetchProducts() {
    this.ps.getAllProducts(this.page, this.pageSize).subscribe(data => this.products = data);
  }


}
