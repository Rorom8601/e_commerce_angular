import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Address, Category } from './entities';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  forEach(arg0: (value: number, index: number) => void) {
    throw new Error('Method not implemented.');
  }

  constructor(private http: HttpClient) { }

  getAddressById(id:number){     
    return this.http.get<Address>(environment.apiUrl+'api/address/'+id);  
  }

  add(address:Address, idAccount:number){
    return this.http.post<Address>(environment.apiUrl+'api/address/'+idAccount, address);
  }

  deleteAddress(id:number){
    return this.http.delete(environment.apiUrl+'api/address/'+id)
  }
  createAddress(address:Address){
    return this.http.post<Address>(environment.apiUrl+'api/address',address)

  }

  updateAddressInList(address:Address,id:number){
    return this.http.put(environment.apiUrl+'api/address/'+id,address);
  }

  getAllSimpleAddressesPerUser(id:number){
    return this.http.get<Address[]>(environment.apiUrl+'api/address/account/'+id);
  }
}
