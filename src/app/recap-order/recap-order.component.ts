import { Component, OnInit } from '@angular/core';
import { Order } from '../entities';
import { OrderService } from '../order.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recap-order',
  templateUrl: './recap-order.component.html',
  styleUrls: ['./recap-order.component.css']
})
export class RecapOrderComponent implements OnInit {

  constructor(private orderService: OrderService, private location: Location,
    private router: Router ) { }

  ngOnInit(): void {
  }

  order: Order = JSON.parse(localStorage.getItem('basket')!);


  validateOrder() {
    this.order.status='1';
    this.orderService.saveOrder(this.order,this.order.account?.id!).subscribe();
    localStorage.removeItem('basket');
    this.router.navigateByUrl('/confirmation');
  }

  goToPreviousPage(){
    this.location.back();
  }

}
