import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Image, Product } from '../entities';
import { ImageService } from "../image.service";
@Component({
  selector: 'app-add-image',
  templateUrl: './add-image.component.html',
  styleUrls: ['./add-image.component.css']
})
export class AddImageComponent implements OnInit {
  form = this.fb.group({
    name: ''
  });
  upload?: File;


  @Output()
  imageCreated = new EventEmitter<Image>();


  constructor(private fb: FormBuilder, private is: ImageService) { }
  images?: string[];
  ngOnInit(): void {
  }
  add() {
    let image: Image = {
      name: this.form.get('name')?.value
    }
    // if (image.name) {
    //   this.images?.push(image.name);
    // }
    // console.log(this.images);

    //this.product?.images.push(image);
    if (this.upload) {
      this.is.add(image, this.upload).subscribe({
        next: (data) => this.imageCreated.emit(data)
      });

    }
  }

  changeFile(event: any) {
    if (event.target.files.length > 0) {
      this.upload = event.target.files[0];
    }
  }
}