import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category } from '../entities';

@Component({
  selector: 'app-admin-category',
  templateUrl: './admin-category.component.html',
  styleUrls: ['./admin-category.component.css']
})
export class AdminCategoryComponent implements OnInit {

  constructor(private service: CategoryService) { }
  categories?:Category[];
  displayEditForm: boolean = false;
  modifiedcategory: Category = {
    id: 0,
    name: '',
  }
  ngOnInit(): void {
    this.service.getAll().subscribe(data=>this.categories=data);
  }

  delete(id:number){
    this.service.deleteCategory(id).subscribe(data =>this.delete);
    this.removeFromList(id);
  }

  removeFromList(id:number){
    this.categories?.forEach((category,category_index)=>{
     if (category.id == id) {
       this.categories?.splice(category_index,1);

     }
    })

  }
  
  categoryId?: number;
  category : Category = {
    id: 0,
    name : ''
  }
  showForm(item:Category){
    this.displayEditForm= true;
    this.categoryId=item.id;
    this.modifiedcategory = item;
  }


  createCategoryByName() {
   this.service.createCategory(this.category).subscribe(data=> this.categories?.push(data))
  }

   update(category:Category) {
    this.service.updateCategoryInList(category,this.categoryId!).subscribe();
    this.updateCategoryInList(this.categories!);
    this.displayEditForm = false;

  }

  updateCategoryInList(category:Category[]) {
    category.forEach((ct_list) => {
      if (ct_list.id == this.category.id) {
        ct_list.name = this.category.name;
        

      }
    });

  }


}
