import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { SingleProductComponent } from './single-product/single-product.component';
import { CategoryComponent } from './category/category.component';
import { ProductsByCategoryComponent } from './products-by-category/products-by-category.component';
import { OrderComponent } from './order/order.component';
import { LoginComponent } from './login/login.component';
import { AddressComponent } from './address/address.component';
import { AdminCategoryComponent } from './admin-category/admin-category.component';
import { RealLoginComponent } from './real-login/real-login.component';
import { AdminProductComponent } from './admin-product/admin-product.component';
import { CreateProductAdminComponent } from './create-product-admin/create-product-admin.component';
import { ProductListComponent } from './product-list/product-list.component';
import { BtnAddItemComponent } from './btn-add-item/btn-add-item.component';
import { BasketComponent } from './basket/basket.component';
import { FinalizeAccountComponent } from './finalize-account/finalize-account.component';
import { AddressCreationComponent } from './address-creation/address-creation.component';
import { HeaderComponent } from './header/header.component';
import { AddImageComponent } from './add-image/add-image.component';
import { ServerFilePipe } from './server-file.pipe';
import { RecapOrderComponent } from './recap-order/recap-order.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { ProductCreationConfimationComponent } from './product-creation-confimation/product-creation-confimation.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SingleProductComponent,
    CategoryComponent,
    ProductsByCategoryComponent,
    OrderComponent,
    LoginComponent,
    AddressComponent,
    AdminCategoryComponent,
    AdminProductComponent,
    CreateProductAdminComponent,
    RealLoginComponent,
    AdminProductComponent,
    ProductListComponent,
    BtnAddItemComponent,
    BasketComponent,
    FinalizeAccountComponent,
    AddressCreationComponent,
    HeaderComponent,
    AddImageComponent,
    ServerFilePipe,
    RecapOrderComponent,
    OrderConfirmationComponent,
    ProductCreationConfimationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
