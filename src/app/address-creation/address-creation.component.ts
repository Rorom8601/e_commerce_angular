import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddressService } from '../address.service';
import { Account, Address, Order } from '../entities';
import { Location } from '@angular/common';

@Component({
  selector: 'app-address-creation',
  templateUrl: './address-creation.component.html',
  styleUrls: ['./address-creation.component.css']
})
export class AddressCreationComponent implements OnInit {

  deliveryAddress?: Address | null;
  billingAddress?: Address | null;
  displayEditForm: boolean = false;
  addresses?: Address[];
  addressId?: number;
  account: Account = JSON.parse(localStorage.getItem("user")!);

  address: Address = {
    id: 0,
    zipCode: "",
    city: "",
    addressDetail: "",
    country: ""
  }

  modifiedAddress: Address = {
    id: 0,
    zipCode: "",
    city: "",
    addressDetail: "",
    country: ""
  }
  constructor(private adService: AddressService, private router: Router, private location: Location) { }

  getAddressById(id: number) {
    this.adService.getAddressById(id).subscribe((data: any) => this.address = data)
  }

  getAllAddress() {
    this.adService.getAllSimpleAddressesPerUser(this.account.id!).subscribe(data => {
      this.addresses = data, console.log(this.addresses);
    });
  }

  ngOnInit(): void {

    this.getAllAddress();
  }

  delete(id: number) {
    this.adService.deleteAddress(id).subscribe(data => this.delete);
    this.removeFromList(id);
  }

  removeFromList(id: number) {
    this.addresses?.forEach((address, address_index) => {
      if (address.id == id) {
        this.addresses?.splice(address_index, 1);

      }
    })

  }
  cancelDeliveryAddress() {
    this.deliveryAddress = null
    console.log(this.billingAddress);
    console.log(this.deliveryAddress);

  }
  cancelBillingAddress() {

    this.billingAddress = null;

  }

  chooseAsDeliveryAddress(address: Address) {
    this.deliveryAddress = address;
  }

  chooseAsBillingAddress(address: Address) {
    this.billingAddress = address;
  }

  validateAddresses() {
    let order: Order = JSON.parse(localStorage.getItem("basket")!);
    order.billingAddress = this.billingAddress;
    order.deliveryAddress = this.deliveryAddress;
    console.log(order);
    localStorage.setItem('basket', JSON.stringify(order)!);
    this.router.navigateByUrl('/recap');
  }


  showForm(item: Address) {
    this.modifiedAddress = item;
    this.displayEditForm = true;
  }


  createAddress() {
    console.log("passage");

    this.account.addresses?.push(this.address);
    //this.address.account = this.account;
    this.adService.add(this.address, this.account.id!).subscribe(data => this.addresses?.push(data));
  }

  update(address: Address) {
    this.adService.updateAddressInList(address, this.addressId!).subscribe();
    this.updateAddressInList(this.addresses!);
    this.displayEditForm = false;

  }

  updateAddressInList(address: Address[]) {
    address.forEach((ad_list) => {
      if (ad_list.id == this.address.id) {
        ad_list.addressDetail = this.address.addressDetail;
        ad_list.zipCode = this.address.zipCode;
        ad_list.city = this.address.city;
        ad_list.country = this.address.country;


      }
    });

  }
  goToPreviousPage() {
    this.location.back();
  }
}
