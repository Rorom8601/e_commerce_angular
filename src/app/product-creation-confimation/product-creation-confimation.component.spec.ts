import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCreationConfimationComponent } from './product-creation-confimation.component';

describe('ProductCreationConfimationComponent', () => {
  let component: ProductCreationConfimationComponent;
  let fixture: ComponentFixture<ProductCreationConfimationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductCreationConfimationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCreationConfimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
