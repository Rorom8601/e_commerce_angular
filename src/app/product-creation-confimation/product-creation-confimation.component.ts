import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-creation-confimation',
  templateUrl: './product-creation-confimation.component.html',
  styleUrls: ['./product-creation-confimation.component.css']
})
export class ProductCreationConfimationComponent implements OnInit {

  constructor(private location:Location) { }

  ngOnInit(): void {
  }

  backToCreation(){
    this.location.back();
  }

}
