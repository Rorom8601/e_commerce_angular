import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category } from '../entities';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private service: CategoryService) { }
  categories?:Category[];
  ngOnInit(): void {
    this.service.getAll().subscribe(data=>this.categories=data);
  }

}
