import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Page, Product } from '../entities';
import { ActivatedRoute, Params } from '@angular/router';
import { switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  //products?: Product[];
  search: boolean = false;
  productsPage?:Page<Product>;
  page = 0;
  pageSize = 10;
  
  constructor(private prodService: ProductService, private route: ActivatedRoute) { }
  ngOnInit(): void {
    this.route.queryParamMap.pipe(
      switchMap(params => {
        if (params.has('search')) {
          return this.prodService.getProductsBySearchField(params.get('search')!,this.page, this.pageSize)
        }
        return this.prodService.getAllProducts(this.page, this.pageSize)
      })
    ).subscribe(data => {
      this.productsPage= data
    });
  }

  changeUrlIfUploadFile(value:string):string{
    if(value.startsWith('http')) {
      return value;
    }
    return environment.apiUrl + 'uploads/' + value;
  }
  

  nextPage() {
    this.page++;
    this.fetchProducts();
  }
  previousPage() {
    this.page--;
    this.fetchProducts();
  }

  fetchProducts() {
    this.prodService.getAllProducts(this.page, this.pageSize).subscribe(data => this.productsPage = data);
  }

}


