import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Account } from './entities';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  updateUser(id:number, account: Account ){
    return this.http.put(environment.apiUrl+'api/account/'+id, account);

  }

}
