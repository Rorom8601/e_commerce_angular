import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddressService } from '../address.service';
import { Address } from '../entities';


@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  address: Address = {
    id: 0,
    zipCode: "",
    city: "",
    addressDetail: "",
    country: ""
  }

  constructor(private adService: AddressService, private router: Router) { }

  ngOnInit(): void {
  }

  save() {

    //this.adService.add(this.address).subscribe(()=> {
    //  this.router.navigate(['/']);
    //});

  }
}
