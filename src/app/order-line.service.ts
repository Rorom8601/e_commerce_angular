import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OrderLine } from './entities';

@Injectable({
  providedIn: 'root'
})
export class OrderLineService {

  constructor(private http:HttpClient) { }

  createOrderLine(orderLine:OrderLine){
    return this.http.post<OrderLine>(environment.apiUrl+'api/orderline',orderLine);
  }


}
