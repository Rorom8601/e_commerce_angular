export interface Product {
  id:number,
  description:string,
  name:string,
  price: number,
  images:Image[],
  longDescription: string,
  category?:Category
}
export interface Image {
  id?:number,
  name?:string,
  url?:string
}
export interface Category {
  id?:number,
  name?:string
  products?:Product[]
}
export interface OrderLine {
  id:number,
  price:number,
  quantity:number,
  product?:Product,
  order?:Order

}
export interface Order {
  id:number,
  orderNumber:number,
  date?:string,
  status:string,
  validated:boolean,
  amount:number,
  orderLines?:OrderLine[],
  account?:Account,
  deliveryAddress?:Address|null,
  billingAddress?:Address|null
}
export interface Account{
  id?:number,
  firstName?:string,
  lastName?:string,
  phoneNumber?:string,
  email?:string,
  password?:string,
  birthDate?:string,
  role?:string,
  orders?:Order[],
  addresses?:Address[] 
}
export interface Address {
  id:number,
  addressDetail:string,
  city:string,
  zipCode:string,
  country:string,
  account?:Account;
}

export interface Page<P> {
  content: P[];
 
  totalPages: number;
  totalElements: number;
  last: boolean;
  first: boolean;

  size: number;
  number: number;
  numberOfElements: number;
  empty: boolean;
}
export interface Cart{
  amount:number,
  lines: OrderLine[]
}
