import { TestBed } from '@angular/core/testing';

import { CredentialInterceptorInterceptor } from './credential-interceptor';

describe('CredentialInterceptorInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      CredentialInterceptorInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: CredentialInterceptorInterceptor = TestBed.inject(CredentialInterceptorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
