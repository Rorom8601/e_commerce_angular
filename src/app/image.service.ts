import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Image } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http: HttpClient) { }
  getAll(){
    return this.http.get<Image[]>(environment.apiUrl+'api/image');
  }

  add(image:Image, upload:File) {
    const form = new FormData();
    form.append('name', image.name!);
    // form.append('url',image.url!)
    form.append('upload', upload);
    return this.http.post<Image>(environment.apiUrl+'api/image/upload', form);
  }

}
