import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../entities';
import { ProductService } from '../product.service';
import { switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-products-by-category',
  templateUrl: './products-by-category.component.html',
  styleUrls: ['./products-by-category.component.css']
})
export class ProductsByCategoryComponent implements OnInit {

  constructor(private route: ActivatedRoute, private serviceForTest:ProductService, private productService:ProductService) { }
products?:Product[];
  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.productService.getProductsByCategoryId(params['id']))
    ).subscribe(data => this.products = data
    );
  }
  changeUrlIfUploadFile(value:string):string{
    if(value.startsWith('http')) {
      return value;
    }
    return environment.apiUrl + 'uploads/' + value;
  }
}
