import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category, Product } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  forEach(arg0: (value: number, index: number) => void) {
    throw new Error('Method not implemented.');
  }

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Category[]>(environment.apiUrl + 'api/category');
  }

  getAllCategoriesWithProduct() {
    return this.http.get<Category[]>(environment.apiUrl + 'api/category');

  }
  updateProductList(id:number, product:Product){
    return this.http.put(environment.apiUrl + 'api/category/update/'+id,product);
  }
  getCategoryById(id:number){
    return this.http.get<Category>(environment.apiUrl+'api/category/'+id);
  }
  deleteCategory(id:number){
    return this.http.delete(environment.apiUrl+'api/category/'+id)
  }
  createCategory(category:Category){
    return this.http.post(environment.apiUrl+'api/category',category)

  }

  updateCategoryInList(category:Category,id:number){
    return this.http.put(environment.apiUrl+'api/category/'+id,category);
  }
}
  