import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnAddItemComponent } from './btn-add-item.component';

describe('BtnAddItemComponent', () => {
  let component: BtnAddItemComponent;
  let fixture: ComponentFixture<BtnAddItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnAddItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
