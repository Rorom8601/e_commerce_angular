import { Component, Input, OnInit } from '@angular/core';
import { OrderLine } from '../entities';

@Component({
  selector: 'app-btn-add-item',
  templateUrl: './btn-add-item.component.html',
  styleUrls: ['./btn-add-item.component.css']
})
export class BtnAddItemComponent implements OnInit {

  @Input()
  orderLineFromParent?:OrderLine;

  constructor() { }


  ngOnInit(): void {
    console.log(this.orderLineFromParent);
    
  }

}
