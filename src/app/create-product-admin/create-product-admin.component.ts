import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../category.service';
import { Category, Image, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-create-product-admin',
  templateUrl: './create-product-admin.component.html',
  styleUrls: ['./create-product-admin.component.css']
})
export class CreateProductAdminComponent implements OnInit {

  categories?: Category[];
  categoryId?: number;
  product: Product = {
    id: 0,
    name: '',
    price: 0,
    description: '',
    images: [],
    longDescription: ''

  };

  constructor(private ps: ProductService, private cs: CategoryService, private route: Router) { }

  ngOnInit(): void {
    this.cs.getAllCategoriesWithProduct().subscribe(data => this.categories = data);

  }
  getImage(image:Image){
    this.product.images.push(image);
   // console.log(this.product);
  }

  createProduct() {
    console.log("création");
    
    console.log(this.categoryId);
    let cat:Category={
      id:this.categoryId,
      name:''
    };
    this.product.category = cat;
    console.log(this.product);
    
   this.ps.create(this.product).subscribe();
   this.goToConfirmationPage();
  }

  goToConfirmationPage(){
    this.route.navigateByUrl('/product-confirmation') 
  }
 
}




