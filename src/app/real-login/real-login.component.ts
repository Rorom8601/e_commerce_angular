import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-real-login',
  templateUrl: './real-login.component.html',
  styleUrls: ['./real-login.component.css']
})
export class RealLoginComponent implements OnInit {
  email:string = '';
  password:string = '';
  hasError = false;

  constructor(private auth:AuthService, private router:Router, private location: Location) { }

  ngOnInit(): void {
  }

  login() {
    this.hasError = false;
    this.auth.login(this.email,this.password).subscribe({
      next: data => this.router.navigate(['/']),
      error: () => this.hasError = true
    });
    
  }
  goToPreviousPage(){
    this.location.back();
  }



}
