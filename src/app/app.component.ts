import { Component } from '@angular/core';
import { Router} from '@angular/router';
import { AuthService } from './auth.service';
import { CartService } from './cart.service';
import { Order, Product } from './entities';
import { HomeComponent } from './home/home.component';
import { ProductService } from './product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'e_commerce_angular';
  pageAdmin:string='';

  constructor(private prodService:ProductService, private router: Router, private auth:AuthService, private cartService: CartService){}

  currentUser=this.auth.currentUser;
  // basket:Order=JSON.parse(localStorage.getItem("basket")!);  
  // basketCreated=this.cartService.order.orderLines?.length!=0;
  field:string='';
  showArticles(){
    let products:Product[];
    console.log("btn actif");
    if(this.field?.length!=0){
      this.router.navigate([''],{queryParams:{search:this.field}});
      this.field='';
    }  
  }
  logout(){
    this.auth.logout().subscribe();
  }
  goToAdminComponent(){  
    if(this.pageAdmin=='createProduct'){
      this.router.navigateByUrl('/create-product-admin');
    }
    if(this.pageAdmin=='modifyProduct'){
      this.router.navigateByUrl('/admin-product');
    }
    if(this.pageAdmin=='categories'){
      this.router.navigateByUrl('/admin-category');
    }
  }
  roleAdmin(){
    if(this.currentUser.user?.role=="ROLE_ADMIN"){
      return true;
    }
    else{
      return false;
    }
  }

}
