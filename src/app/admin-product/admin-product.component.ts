import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Page, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.css']
})
export class AdminProductComponent implements OnInit {

   products?:Page<Product>;

  
  displayEditForm: boolean = false;
  modifiedProduct: Product = {
    id: 0,
    name: '', 
    price: 0,
    description: '',
    images: [],
    longDescription: ''
  };

  constructor(private prodService: ProductService) { }

  ngOnInit(): void {
    this.prodService.getAllProducts().subscribe(data => this.products = data);
  }

  delete(id:number) {
    this.prodService.deleteProduct(id).subscribe();
    this.removeFromList(id);
  }
  removeFromList(id: number) {
    this.products?.content.forEach((product, product_index) => {
      if (product.id == id) {
        this.products?.content.splice(product_index, 1);
      }
    });
  }
  
  showEditForm(product:Product) {
    this.displayEditForm = true;
    this.modifiedProduct = Object.assign({}, product)
  }

  update(product:Product) {
    this.prodService.put(product).subscribe();
    this.updateProductInList(product);
    this.displayEditForm = false;

  }

  updateProductInList(product:Product) {
    this.products?.content.forEach((pr_list) => {
      if (pr_list.id == product.id) {
        pr_list.description = product.description;
        pr_list.images = product.images;
        pr_list.longDescription = product.longDescription;
        pr_list.name = product.name;
        pr_list.price = product.price;

      }
    });

  }
  
}