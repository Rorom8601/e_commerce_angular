import { Component, OnInit } from '@angular/core';
import { Order, OrderLine } from '../entities';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

  }
  order: Order = JSON.parse(localStorage.getItem("basket")!);

  deleteOrderLine(OrderLine: OrderLine) {
    // let index = this.order.orderLines?.findIndex(data => data.product!.id = id);
    // let amountToSubstract = Number(this.order.orderLines![index!].price) * Number(this.order.orderLines![index!].quantity);
    // this.order.orderLines?.splice(index!, 1);
    // this.order.amount -= amountToSubstract;
    const index = this.order.orderLines!.indexOf(OrderLine);
    let amountToSubstract = Number(this.order.orderLines![index!].price) * Number(this.order.orderLines![index!].quantity);
    this.order.orderLines?.splice(index!, 1);
    this.order.amount -= amountToSubstract;
    this.saveBasket()
  }

 

  updateAmount(){
    let amount=0;
    for (const item of this.order.orderLines!) {
        amount+=item.price*item.quantity
    }
    this.order.amount=amount;
    this.saveBasket();
  }
  saveBasket(){
    localStorage.setItem('basket', JSON.stringify(this.order));
  }

}
