import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Order } from './entities';

@Injectable({
  providedIn: 'root'
})
export class OrderService {


  

  constructor(private http: HttpClient) { }


  saveOrder(order:Order, idAccount:number){
    return this.http.post<void>(environment.apiUrl+'api/order/'+idAccount,order);
  }
}
