import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderLine, Product } from '../entities';
import { ProductService } from '../product.service';
import { switchMap } from 'rxjs';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit {
  quantite: number = 0;
  product?: Product;
  orderLine={
    id:0,
    quantity:0,
    price:0,
    product:this.product
  }


  constructor(private route: ActivatedRoute, private service: ProductService, private router: Router, private basketService: CartService) { }

  showQuantity() {
    console.log(this.quantite);
  }
  // redirectWithParams() {
  //   this.router.navigate(['/order', this.product?.id, this.quantite]);
  // }

  buildOrderLine(){
    this.orderLine.quantity=this.quantite;
    this.orderLine.product=this.product; //Fait doublon je pense à revoir
    this.orderLine.price=this.product!.price;
   // console.log(this.orderLine);
   
  }

  addOrderLine(){
    this.basketService.createOrUpdateBasket(this.orderLine);
    // Vérifier si l'id du produit est déja présent dans l'order, ici ou plus sûrement dans la fonction
  }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.service.getProductById(params['id']))
    ).subscribe(data => this.product = data
    );

  }

}
