import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Account } from '../entities';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  account:Account={
  id:0,
  firstName:'temp',
  lastName:'temp',
  phoneNumber:'temp',
  email:'',
  password:'',
  birthDate:'1900-01-01',
  role:''
}
  hasError=false;
  passwordRepeat?:string;

  constructor(private router:Router, private authService:AuthService) { }

  register(){
    this.hasError=false;
    console.log(this.account);
    
    this.authService.Register(this.account).subscribe({
      next: data => this.router.navigate(['/']),
      error: () => this.hasError = true
    })
  }
 

  ngOnInit(): void {
  }

}
