import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Account } from './entities';


export interface CurrentUser {
  user: Account | null;
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser: CurrentUser = {
    user: null
  };

  constructor(private http: HttpClient) {
    const stored = localStorage.getItem('user');
    if (stored) {
      this.currentUser.user = JSON.parse(stored);
    }

  }

  Register(account: Account) {
    return this.http.post<Account>(environment.apiUrl + 'api/account', account).pipe(
      tap(data => this.updateUser(data))

      //appeler updateUser???
    )
  }

  logout() {
    return this.http.get<void>(environment.apiUrl +'logout').pipe(
      tap(() => this.updateUser(null))
    );
  }


  login(email: string, password: string) {
    return this.http.get<Account>(environment.apiUrl + 'api/account/user', {
      headers: {
        'Authorization': 'Basic ' + btoa(email + ':' + password),
      }
    }).pipe(
      tap(data => this.updateUser(data)), //On assigne user connectée au state
    );
  }

  public updateUser(data: Account | null) {
    this.currentUser.user = data;
    if (data) {
      localStorage.setItem('user', JSON.stringify(data));
    } else {
      localStorage.removeItem('user');
    }
  }



}
