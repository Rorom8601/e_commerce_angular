import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressCreationComponent } from './address-creation/address-creation.component';
import { AddImageComponent } from './add-image/add-image.component';
import { AddressComponent } from './address/address.component';
import { AdminCategoryComponent } from './admin-category/admin-category.component';
import { AdminProductComponent } from './admin-product/admin-product.component';
import { BasketComponent } from './basket/basket.component';
import { CategoryComponent } from './category/category.component';
import { CreateProductAdminComponent } from './create-product-admin/create-product-admin.component';
import { FinalizeAccountComponent } from './finalize-account/finalize-account.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { OrderComponent } from './order/order.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductsByCategoryComponent } from './products-by-category/products-by-category.component';
import { RealLoginComponent } from './real-login/real-login.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { RecapOrderComponent } from './recap-order/recap-order.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { ProductCreationConfimationComponent } from './product-creation-confimation/product-creation-confimation.component';

const routes: Routes = [
{path:'', component:HomeComponent},
{path:'home/:search',component:HomeComponent},
{path:'product/:id', component: SingleProductComponent},
{path:'category',component:CategoryComponent},
{path:'category/:id',component:ProductsByCategoryComponent},
{path:'order/:id', component:OrderComponent},
{path:'address',component:AddressComponent},
{path:'admin-product',component:AdminProductComponent},
{path:'create-product-admin',component:CreateProductAdminComponent},
{path:'login',component:LoginComponent},
{path:'adress',component:AddressComponent},
{path:'order/:id/:qte', component:OrderComponent},
{path:'admin-category',component:AdminCategoryComponent},
{path:'connect',component:RealLoginComponent},
{path:'product-list',component:ProductListComponent},
{path:'address-creation',component:AddressCreationComponent},
{path:'basket',component:BasketComponent},
{path:'finalize', component:FinalizeAccountComponent},
{path:'add-image',component:AddImageComponent},
{path:'recap',component:RecapOrderComponent},
{path:'confirmation', component:OrderConfirmationComponent},
{path:'product-confirmation',component:ProductCreationConfimationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
