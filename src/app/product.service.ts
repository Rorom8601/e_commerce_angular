import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category, Page, Product } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getAllProducts(page = 0, pageSize = 10) {
    return this.http.get<Page<Product>>(environment.apiUrl + 'api/product', {
      params: { page, pageSize }
    });
  }
  getProductById(id: number) {
    return this.http.get<Product>(environment.apiUrl + 'api/product/' + id);
  }

  create(product: Product) {
    return this.http.post<Product>(environment.apiUrl + 'api/product', product);
  }


  deleteProduct(id: number) {
    return this.http.delete(environment.apiUrl + 'api/product/' + id);
  }

  put(product: Product) {
    return this.http.put<Product>(environment.apiUrl + 'api/product/' + product.id, product);
  }
  getProductsByCategoryId(id: number) {
    return this.http.get<Product[]>(environment.apiUrl + 'api/product/category/' + id);
  }
  getProductsBySearchField(input: string, page = 0, pageSize = 10) {
    return this.http.get<Page<Product>>(environment.apiUrl + "api/product/search/" + input,{
      params: { page, pageSize }
    });
  }
}
