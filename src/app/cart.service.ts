import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Order, OrderLine } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http: HttpClient, private router: Router) {
    // const storedBasket = localStorage.getItem("basket");
    // if (storedBasket) {
    //   this.order = JSON.parse(storedBasket);
    // }
  }

  index: number = 0;

  get listOrderLines() {
    return this.order.orderLines;
  }

  createOrUpdateBasket(orderLine: OrderLine) {
    //this.order = JSON.parse(localStorage.getItem("basket")!);
    if (localStorage.getItem('user') == null) {
      this.router.navigate(['/connect'])
    }
    else if (localStorage.getItem("basket") == null ) {
      this.order.amount = orderLine.price * orderLine.quantity;
      this.order.orderLines?.push(orderLine);
      this.order.account = JSON.parse(localStorage.getItem('user')!);
      localStorage.setItem('basket', JSON.stringify(this.order));
      this.router.navigate(['/basket']);
      //gérer les cas ou le panier ne correspond pas au user, l'enregistrer et le supprimer quand il se déconncete?
    }

    else if ((this.orderLineAlreadyPresentInOrder(orderLine)) != -1) {
      this.order = JSON.parse(localStorage.getItem("basket")!);//Pas sûr que ce soit utile
      this.order.amount += orderLine.price * orderLine.quantity;
      let newQuantity: number = Number(this.order.orderLines![this.index].quantity) + Number(orderLine.quantity);
      this.order.orderLines![this.index].quantity = newQuantity;
      localStorage.setItem('basket', JSON.stringify(this.order));
      this.router.navigate(['/basket']);
    }
    else {
      this.order = JSON.parse(localStorage.getItem("basket")!);//Pas sûr que ce soit utile
      this.order.amount += orderLine.price * orderLine.quantity;
      this.order.orderLines?.push(orderLine);
      localStorage.setItem('basket', JSON.stringify(this.order));
      this.router.navigate(['/basket']);

    }
  }

  orderLineAlreadyPresentInOrder(orderLine: OrderLine): number {

    return this.order.orderLines!.findIndex(data => data.product?.id == orderLine.product?.id);
  }


  order: Order = {
    id: 0,
    orderNumber: 0,
    date: '',
    status: '',
    validated: false,
    amount: 0,
    orderLines: [],
    deliveryAddress: null,
    billingAddress: null
  }
  orderLine = {
    id: 0,
    quantity: 0,
    price: 0,
    product: []
  }


}
