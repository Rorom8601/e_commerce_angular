import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { OrderLine, Product } from '../entities';
import { OrderLineService } from '../order-line.service';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  constructor(private route: ActivatedRoute, private orderLineService: OrderLineService, private productService: ProductService) { }

  printId() {
    console.log(this.idProduct);
  }

  product?: Product;
  orderL?: OrderLine;
  productId = 1;
  quantite=0;

  saveOrderLine() {
    this.productService.getProductById(1).subscribe(data => this.product = data);
    this.orderL = {
      id: 10,
      quantity: 1,
      price: 120.00,
      product: this.product
    }
    this.orderLineService.createOrderLine(this.orderL).subscribe();
  }

  idProduct?: number;
  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.productId = (params['id']))
    );
    this.route.params.pipe(
      switchMap(params => this.quantite = (params['qte']))
    )
  }

}
