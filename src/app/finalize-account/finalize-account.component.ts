import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../account.service';
import { AuthService } from '../auth.service';
import { Account } from '../entities';
import { OrderService } from '../order.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-finalize-account',
  templateUrl: './finalize-account.component.html',
  styleUrls: ['./finalize-account.component.css']
})
export class FinalizeAccountComponent implements OnInit {

  constructor(private fb: FormBuilder, private location:Location, private accountService: AccountService, private authService: AuthService) { }

  ngOnInit(): void {
    this.checkIfAccountIsFinalize();
  }
  account: Account = JSON.parse(localStorage.getItem("user")!);
  accountCompleted = false;

  registerForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    phoneNumber: ['', Validators.required],
    birthDate: ['', Validators.required]
  })

  modifiyAccount(){
    console.log(this.account.firstName);
    
    this.accountCompleted = false;
    this.registerForm.setValue({firstName:this.account.firstName,lastName:this.account.lastName, phoneNumber:this.account.phoneNumber, birthDate:this.account.birthDate});
  }

  checkIfAccountIsFinalize() {
    if (this.account.firstName == "temp") {
      this.accountCompleted = false
    }
    else{
    this.accountCompleted = true;
  }
  }

  register() {
    let accountCompleted: Account = {
      firstName: this.registerForm.get('firstName')?.value,
      lastName: this.registerForm.get('lastName')?.value,
      phoneNumber: this.registerForm.get('phoneNumber')?.value,
      birthDate: this.registerForm.get('birthDate')?.value,
      email:this.account.email,
      password:this.account.password,
      id:this.account.id
    }
    let userId: number = this.authService.currentUser.user!.id!;
    this.accountService.updateUser(userId, accountCompleted).subscribe();
    this.authService.updateUser(accountCompleted);
    this.accountCompleted = true;
    this.account=accountCompleted;
  }

  checkValues() {
    console.log(this.registerForm);
    console.log(this.registerForm.get('firstName')?.value);
    console.log(this.registerForm.get('lastName')?.value);
    console.log(this.registerForm.get('phoneNumber')?.value);
    console.log(this.authService.currentUser.user!.id!);
    console.log(this.registerForm.get('birthDate')?.value);
  }
  goToPreviousPage(){
    this.location.back();
  }
}








